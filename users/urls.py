from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns('',
    url(r'^$', views.DashboardView.as_view(), name="dashboard"),
    url(r'^register/$', views.UserCreateView.as_view(), name="create"),
    url(r'^profile/update/$', views.ProfileUpdateView.as_view(), name="update"),
    url(r'^signin/$', views.LoginView.as_view(), name="login"),
    url(r'^logout/$', views.LogoutView.as_view(), name="logout"),
)
