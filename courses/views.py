from django.views.generic import ListView, DetailView, CreateView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect

from model_urls.urlresolvers import reverse

from default.utils import view_decorator

from .models import Course, Participation, Category, Comment
from .forms import CourseCreateForm, CommentForm


class CategoryListMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['categories'] = Category.objects.all()
        return context


class CourseListView(CategoryListMixin, ListView):
    model = Course
    context_object_name = 'courses'


class CategoryView(CategoryListMixin, DetailView):
    model = Category
    context_object_name = 'category'
    template_name = "courses/course_list.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['courses'] = self.object.courses.all()
        return context


class CourseDetailView(DetailView):
    model = Course

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['comment_form'] = CommentForm()
        return context


class CourseCreateView(SuccessMessageMixin, CreateView):
    model = Course
    form_class = CourseCreateForm
    success_message = "Votre cours a bien été créé."

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['author'] = self.request.user
        kwargs['initial'] = {'category': self.get_initial_category()}
        return kwargs

    def form_valid(self, form):
        parent = super().form_valid(form)
        Participation.objects.create(user=self.request.user,
                                     course=form.instance, is_owner=True)
        return parent

    def get_success_url(self):
        return reverse('courses:list')

    def get_initial_category(self):
        category_params = self.request.GET.get('category', "").split('_')
        return category_params[1] if len(category_params) > 1 else None


@view_decorator(login_required)
class ParticipationMixin(SingleObjectMixin, RedirectView):
    model = Course
    permanent = False

    def get(self, request, *args, **kwargs):
        self.update_participation(course=self.get_object(), user=request.user)
        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('courses:details', self.get_object())


class CourseJoinView(ParticipationMixin):
    def update_participation(self, course, user):
        Participation.objects.create(user=user, course=course)


class CourseLeaveView(ParticipationMixin):
    def update_participation(self, course, user):
        try:
            Participation.objects.get(user=user, course=course).delete()
        except Participation.DoesNotExist:
            pass


class CommentFormView(SuccessMessageMixin, CreateView):
    model = Comment
    form_class = CommentForm
    success_message = "Votre commentaire a bien été publié."

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['course'] = self.get_course()
        return kwargs

    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        return HttpResponseRedirect(self.request.GET.get(
            'HTTP_REFERER', self.get_success_url()))

    def get_success_url(self):
        return reverse('courses:details', self.get_course())

    def get_course(self):
        return self.get_object(Course.objects.all())
