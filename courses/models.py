from django.db import models
from django.conf import settings
from default.utils import unique_slugify
from django.dispatch import receiver


User = settings.AUTH_USER_MODEL


class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('slug',)


class Course(models.Model):
    category = models.ForeignKey(Category, related_name="courses")

    slug = models.SlugField()
    SLUG_FROM = 'title'
    title = models.CharField(max_length=100)
    body = models.TextField(null=True)

    schedule = models.CharField(max_length=255, null=True)
    cost = models.PositiveIntegerField(null=True)
    students_count = models.PositiveIntegerField(default=0)
    min_students_count = models.PositiveIntegerField(default=2)
    max_students_count = models.PositiveIntegerField(default=8)

    city = models.CharField(max_length=100, null=True)
    zipcode = models.CharField(max_length=6, null=True)
    distance = models.PositiveIntegerField(default=10)

    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True)

    participants = models.ManyToManyField(User,
                                          through='Participation',
                                          related_name='courses')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-creation_date',)

    def is_participant(self, user):
        return user in self.participants.all()

    def is_owner(self, user):
        try:
            participation = Participation.objects.get(course=self,
                                                      is_owner=True)
            return participation.user == user
        except Participation.DoesNotExist:
            return False

    @property
    def category_slug(self):
        return self.category.slug


@receiver(models.signals.pre_save, sender=Course)
def auto_slug(instance, **kwargs):
    unique_slugify(instance, getattr(instance, getattr(instance, 'SLUG_FROM')))


class Participation(models.Model):
    """
    Who participates to what course with which role.
    """
    ROLES = ('student', 'teacher',)
    STATUSES = ('pending', 'granted', 'denied',)
    course = models.ForeignKey(Course, related_name='participations')
    user = models.ForeignKey(User, related_name='participations')
    is_owner = models.BooleanField(default=False)
    role = models.CharField(max_length=20, choices=tuple(zip(ROLES, ROLES)),
                            default=ROLES[0])
    status = models.CharField(max_length=20,
                              choices=tuple(zip(STATUSES, STATUSES)),
                              default=STATUSES[0])
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s at %s" % (self.user, self.course)

    class Meta:
        unique_together = [('course', 'user')]
        ordering = ('-creation_date',)


class Comment(models.Model):
    user = models.ForeignKey(User, related_name='comments')
    course = models.ForeignKey(Course, related_name='comments')
    body = models.TextField()
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s commented on %s" % (self.user, self.course)

    class Meta:
        ordering = ('-creation_date',)
