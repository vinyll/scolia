# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courses', '0004_auto_20141018_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('body', models.TextField()),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('course', models.ForeignKey(to='courses.Course', related_name='comments')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='comments')),
            ],
            options={
                'ordering': ('-creation_date',),
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ('slug',)},
        ),
        migrations.AlterModelOptions(
            name='participation',
            options={'ordering': ('-creation_date',)},
        ),
        migrations.AlterField(
            model_name='participation',
            name='course',
            field=models.ForeignKey(to='courses.Course', related_name='participations'),
        ),
        migrations.AlterField(
            model_name='participation',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='participations'),
        ),
    ]
