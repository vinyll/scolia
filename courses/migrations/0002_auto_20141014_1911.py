# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='distance',
            field=models.PositiveIntegerField(default=10),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='course',
            name='min_students_count',
            field=models.PositiveIntegerField(default=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='course',
            name='max_students_count',
            field=models.PositiveIntegerField(default=8),
        ),
    ]
