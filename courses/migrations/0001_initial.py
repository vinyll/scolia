# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('slug', models.SlugField()),
                ('title', models.CharField(max_length=100)),
                ('body', models.TextField(null=True)),
                ('schedule', models.CharField(null=True, max_length=255)),
                ('cost', models.PositiveIntegerField(null=True)),
                ('students_count', models.PositiveIntegerField(default=0)),
                ('max_students_count', models.PositiveIntegerField(default=1)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('modification_date', models.DateTimeField(auto_now=True)),
                ('city', models.CharField(null=True, max_length=100)),
                ('zipcode', models.CharField(null=True, max_length=6)),
                ('author', models.ForeignKey(related_name='courses', to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(related_name='courses', to='courses.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
