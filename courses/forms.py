from django import forms

from .models import Course, Comment


class CourseCreateForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = ('category', 'title', 'body', 'city')
        #exclude = ('author', 'slug', 'students_count', 'participants')

    def __init__(self, *args, **kwargs):
        self.author = kwargs.pop('author')
        return super().__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.instance.author = self.author
        super().save(*args, **kwargs)


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        exclude = ('course', 'user',)

    def __init__(self, *args, **kwargs):
        if 'user' in kwargs:
            self.user = kwargs.pop('user')
        if 'course' in kwargs:
            self.course = kwargs.pop('course')
        return super().__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.instance.user = self.user
        self.instance.course = self.course
        super().save(*args, **kwargs)
