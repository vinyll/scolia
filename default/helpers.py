# -*- coding: utf-8 -*-
from jingo import register
from jingo.helpers import datetime


@register.filter
def date(t):
    return datetime(t, fmt="%d %b %Y")


@register.filter
def date_time(t):
    return datetime(t, fmt="%d %b %Y à %H:%m")


@register.filter
def time(t):
    return datetime(t, fmt="%H:%m")
